<%-- 
    Document   : salida
    Created on : 14-04-2020, 21:33:40
    Author     : rlabarca
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="bootstrap.min.css" rel="stylesheet" type="text/css" />
        <title>JSP Page</title>
    </head>
    <body>
        <% 
            String mensaje = (String) request.getAttribute("mensajeSalida");
        %>
        <table class="table">
                <%=mensaje%>
            <tr>
                <td class="text-center"><a class="btn btn-primary" align="center" href="index.jsp" target="_top">Volver</a></td>
                
            </tr>
        </table>
        
        
    </body>
</html>
