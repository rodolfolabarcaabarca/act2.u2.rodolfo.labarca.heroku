<%-- 
    Document   : index
    Created on : 14-04-2020, 21:03:17
    Author     : rlabarca
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="bootstrap.min.css" rel="stylesheet" type="text/css" />
        <title>JSP Page</title>
    </head>
    <body>
        <h3 class="text-center">Ingrese Datos Nuevo Vehiculo:</h3>
        <h6 class="text-center">* TODOS LOS CAMPOS SON REQUERIDOS.</h6>
        <div class="container">
        <form action="controller" method="POST">
            <table class="table table-hover">
                <tr>
                    <th>Patente (PPU): </th>
                    <td><input type="text" name="patente" value="" size="8" maxlength="6" required/></td>
                </tr>
                <tr>
                    <th>Marca: </th>
                    <td><input type="text" name="marca" value="" size="25" maxlength="25" required/></td>
                </tr>
                <tr>
                    <th>Modelo: </th>
                    <td><input type="text" name="modelo" value="" size="25" maxlength="25" required/></td>
                </tr>
                <tr>
                    <th>Año: </th>
                    <td><input type="number" name="anho" value="" size="6" maxlength="4" required/></td>
                </tr>
                <tr>
                    <th>Kilometraje: </th>
                    <td><input type="number" name="kilometraje" value="" size="8" maxlength="6" required/></td>
                </tr>
                <tr>
                    <th>Rut Propietario: </th>
                    <td><input type="text" name="rut_duenho" value="" size="14" maxlength="12" required/></td>
                </tr>
                <tr>
                    <th>
                        <input type="text" hidden="true" name="accion" value="add" />
                        
                    </th>
                    <td>
                        <input type="submit" class="btn btn-primary" value="Ingresar" />
                        <input type="reset" class="btn btn-info" value="Limpiar" />
                        <a href="index.jsp" target="_top" class="btn btn-danger ">Cancelar</a>
                    </td>
                </tr>
            </table>     
        </form>
        </div>
    </body>
</html>
