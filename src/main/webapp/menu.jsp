<%-- 
    Document   : menu
    Created on : 15-04-2020, 1:51:26
    Author     : rlabarca
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="bootstrap.min.css" rel="stylesheet" type="text/css" />
        <title>JSP Page</title>
    </head>
    <body>
        <div class="container">
            <h2 class="text-center">* Registro de Vehiculos *</h2>
            <h4 class="text-center">Seleccione una opcion:</h4>
            <table class="table table-bordered">
                <tr class="text-center">
                    <td class="text-center">
                        <a href="ingreso.jsp" target="principal" class="btn btn-success btn-sm">Ingresar Vehiculo</a>
                        <a href="lista.jsp" target="principal" class="btn btn-info btn-sm">Listar Vehiculos</a>
                    </td>
                </tr>
            </table>
            
        </div>
    </body>
</html>
