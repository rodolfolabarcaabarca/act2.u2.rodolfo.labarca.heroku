<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.List"%>
<%@page import="root.model.entities.IngVehiculo"%>
<%@page import="root.model.dao.IngVehiculoDAO"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="bootstrap.min.css" rel="stylesheet" type="text/css" />
        <title>JSP Page</title>
    </head>
    <body>
        <%
            IngVehiculoDAO dao = new IngVehiculoDAO();
            String patente = (String) request.getParameter("ppu");
 
            dao.destroy(patente);
        %>
        <%-- Fomulario de Edicion --%>
        <table class="table">
            <tr>
                <th><h3 class="text-center">Vehiculo Eliminado Correctamente</h3></th>
            </tr>
            <tr>
                <td class="text-center"><a class="btn btn-primary" align="center" href="index.jsp" target="_top">Volver</a></td>
            </tr>
        </table>
        
    </body>
</html>
