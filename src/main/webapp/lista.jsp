<%-- 
    Document   : lista
    Created on : 15-04-2020, 1:51:47
    Author     : rlabarca
--%>

<%@page import="java.util.List"%>
<%@page import="root.model.entities.IngVehiculo"%>
<%@page import="root.model.dao.IngVehiculoDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="bootstrap.min.css" rel="stylesheet" type="text/css" />
        <script src="jquery-2.0.3.min.js"></script>
        <script src="bootstrap.min.js"></script>
        <title>JSP Page</title>
    </head>
    <body>
        <%-- Popup de Confirmación de Eliminacion --%>
        <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Confirmar Eliminación</h4>
                </div>
            
                <div class="modal-body">
                    <p>Esta a punto de Eliminar este registro .</p>
                    <p>Esta seguro que desea continuar?</p>
                </div>
                
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cancelar</button>
                    <a class="btn btn-danger btn-ok">Eliminar</a>
                </div>
            </div>
        </div>
        </div>
        <%-- FIN --%> 
        <%
            IngVehiculoDAO dao = new IngVehiculoDAO();  
            int cantidadVehiculos = dao.getIngVehiculoCount();            
        %>
        <DIV class="container">
            <h5>Vehiculos Registrados: <%=cantidadVehiculos%> </h5>
            <hr> 
            <table class="table table-bordered">
                <tr>
                    <th class="text-center">Patente</th>
                    <th class="text-center">Marca</th>
                    <th class="text-center">Modelo</th>
                    <th class="text-center">Año</th>
                    <th class="text-center">Kilometraje</th>
                    <th class="text-center">Rut Dueño</th>
                </tr>


            <%
                          
                List<IngVehiculo> getVehiculos = dao.findIngVehiculoEntities();
                int indice = cantidadVehiculos - 1; 
                if (indice <0){
                    indice = 0; 
                }
                if (cantidadVehiculos>0) {
                    for(int i=0;i<=indice;i++) {
                        out.write("<tr>");
                        out.write("<td class='text-center'>" + getVehiculos.get(i).getPatente() + "</td>");
                        out.write("<td class='text-center'>" + getVehiculos.get(i).getMarca() + "</td>");
                        out.write("<td class='text-center'>" + getVehiculos.get(i).getModelo() + "</td>");
                        out.write("<td class='text-center'>" + getVehiculos.get(i).getAnho() + "</td>");
                        out.write("<td class='text-center'>" + getVehiculos.get(i).getKms() + "</td>");
                        out.write("<td class='text-center'>" + getVehiculos.get(i).getRutDuenho() + "</td>");
                        out.write("<td class='text-center'>");
                        out.write("<a href='modificar.jsp?ppu=" + getVehiculos.get(i).getPatente() + "' class='btn btn-warning btn-sm'>Editar</a>  ");
                        out.write("<a href='#' data-href='eliminar.jsp?ppu=" + getVehiculos.get(i).getPatente() + "' data-toggle='modal' data-target='#confirm-delete' class='btn btn-danger btn-sm'>Eliminar</a>  ");
                        out.write("</td>");
                        out.write("</tr>");
                    }    
                } else {
                     out.write("<tr>");
                     out.write("<td class='text-center'> NO EXISTEN VEHICULOS REGISTRADOS </td> ");
                     out.write("</tr>");
                }    
                
            %>
            </table>
        </DIV>
        <script>
        $('#confirm-delete').on('show.bs.modal', function(e) {
            $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
            
            $('.debug-url').html('Delete URL: <strong>' + $(this).find('.btn-ok').attr('href') + '</strong>');
        });
    </script>
    </body>
</html>     
