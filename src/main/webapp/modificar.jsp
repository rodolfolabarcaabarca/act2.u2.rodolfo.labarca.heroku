<%-- 
    Document   : modificar
    Created on : 15-04-2020, 10:43:55
    Author     : rlabarca
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.List"%>
<%@page import="root.model.entities.IngVehiculo"%>
<%@page import="root.model.dao.IngVehiculoDAO"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="bootstrap.min.css" rel="stylesheet" type="text/css" />
        <title>JSP Page</title>
    </head>
    <body>
        <%
            IngVehiculoDAO dao = new IngVehiculoDAO();
            String patente = (String) request.getParameter("ppu");
            IngVehiculo modVehiculo = dao.findIngVehiculo(patente); 
        
        %>
        <%-- Fomulario de Edicion --%>
        <h3 class="text-center">Editando datos de Patente: <%=patente%></h3>
        <h6 class="text-center">* TODOS LOS CAMPOS SON REQUERIDOS</h6>
        <div class="container">
        <form action="controller" method="POST">
            <table class="table table-hover">
                <tr>
                    <th>Patente (PPU): </th> 
                    <td><input type="text" name="patente" value="<%=modVehiculo.getPatente()%>" size="8" maxlength="6" readonly/> <i>-No Editable, Debe Eliminar PPU -</i></td>
                </tr>
                <tr>
                    <th>Marca: </th>
                    <td><input type="text" name="marca" value="<%=modVehiculo.getMarca()%>" size="25" maxlength="25" required/></td>
                </tr>
                <tr>
                    <th>Modelo: </th>
                    <td><input type="text" name="modelo" value="<%=modVehiculo.getModelo()%>" size="25" maxlength="25" required/></td>
                </tr>
                <tr>
                    <th>Año: </th>
                    <td><input type="number" name="anho" value="<%=modVehiculo.getAnho()%>" size="6" maxlength="4" required/></td>
                </tr>
                <tr>
                    <th>Kilometraje: </th>
                    <td><input type="number" name="kilometraje" value="<%=modVehiculo.getKms()%>" size="8" maxlength="6" required/></td>
                </tr>
                <tr>
                    <th>Rut Propietario: </th>
                    <td><input type="text" name="rut_duenho" value="<%=modVehiculo.getRutDuenho()%>" size="14" maxlength="12" required/></td>
                </tr>
                <tr>
                    <th>
                        <input type="text" hidden="true" name="accion" value="mod" />
                        
                    </th>
                    <td>
                        <input type="submit" class="btn btn-primary" value="Editar" />
                        <a href="index.jsp" target="_top" class="btn btn-danger ">Cancelar</a>
                    </td>
                </tr>
            </table>     
        </form>
        </div>
    </body>
</html>
