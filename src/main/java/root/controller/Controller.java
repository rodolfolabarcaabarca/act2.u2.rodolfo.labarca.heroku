/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import root.model.dao.IngVehiculoDAO;
import root.model.entities.IngVehiculo;

/**
 *
 * @author rlabarca
 */
@WebServlet(name = "Controller", urlPatterns = {"/controller"})
public class Controller extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        /** Captura de Accion a Relizar **/ 
        String accion = request.getParameter("accion");
        /** Captura de Variables de Formulario **/
        String patente = request.getParameter("patente"); 
        String marca = request.getParameter("marca"); 
        String modelo = request.getParameter("modelo"); 
        String anho = request.getParameter("anho");
        String kilometraje = request.getParameter("kilometraje");
        String rut_duenho = request.getParameter("rut_duenho"); 

        /** String a Int **/ 
        int fixAnho = Integer.parseInt(anho);
        int fixKilometraje = Integer.parseInt(kilometraje); 
        
        /** Creamos Entity **/  
        IngVehiculo vehiculo = new IngVehiculo(); 
        vehiculo.setPatente(patente);
        vehiculo.setMarca(marca);
        vehiculo.setModelo(modelo);
        vehiculo.setAnho(fixAnho);
        vehiculo.setKms(fixKilometraje);
        vehiculo.setRutDuenho(rut_duenho);

        /** Objeto DAO **/
        IngVehiculoDAO dao = new IngVehiculoDAO();
        
        /** Variable de Mensaje de Salida **/
        String mensajeSalida = ""; 
        
        /** Objeto para Validar si Existe **/
        
        /** CAPTURA DE ACCION **/
        switch(accion) {
            case "add":
                    try {                      
                        dao.create(vehiculo);
                        Logger.getLogger("log").info("valor patente Ingresado: {0}"+ vehiculo.getPatente());
                        mensajeSalida = "<tr class='text-center'><th class='text-center'><h3 class='text-center'>Vehiculo Patente: " + vehiculo.getPatente() + " Se ha ingresado correctamente.</h3></th>";
                    } catch (Exception ex) {
                        Logger.getLogger("log").log(Level.SEVERE,"Error al ingresar la PPU . {0}"+ ex.getMessage());
                        mensajeSalida = "<tr class='text-center'><th class='text-center'><h3 class='text-center'>Patente " + vehiculo.getPatente() + " Existe</h3></th></tr><tr><td class='text-center'><p>¿Que desea hacer?</p><a href='modificar.jsp?ppu=" + vehiculo.getPatente() + "'' class='btn btn-warning'>Editar</a>  <a href='eliminar.jsp?ppu=" + vehiculo.getPatente() + "' class='btn btn-danger'>Eliminar</a></td></tr>";
                    }
                    /** Salidas Resultado Registro**/ 
                    
                    request.setAttribute("mensajeSalida", mensajeSalida);
                    request.getRequestDispatcher("salida.jsp").forward(request, response);
                break; 
            case "mod":
                    try {
                            dao.edit(vehiculo);
                            Logger.getLogger("log").info("valor patente Modificado: {0}"+ vehiculo.getPatente());
                        } catch (Exception ex) {
                            Logger.getLogger("log").log(Level.SEVERE,"Error al modificar la PPU . {0}"+ ex.getMessage());  
                        }

                    /** Salidas Resultado Registro**/
                    mensajeSalida = "<tr class='text-center'><th class='text-center'><h3 class='text-center'>Vehiculo Patente: " + vehiculo.getPatente() + " Se ha modificado correctamente.</h3></th>"; 
                    request.setAttribute("mensajeSalida", mensajeSalida);
                    request.getRequestDispatcher("salida.jsp").forward(request, response);
                break;
            default: 
                throw new AssertionError();
        }
        
        
        
     
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
