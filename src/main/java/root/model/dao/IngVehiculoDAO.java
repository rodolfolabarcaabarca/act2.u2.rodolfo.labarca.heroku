/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.model.dao;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import root.model.dao.exceptions.NonexistentEntityException;
import root.model.dao.exceptions.PreexistingEntityException;
import root.model.entities.IngVehiculo;

/**
 *
 * @author rlabarca
 */
public class IngVehiculoDAO implements Serializable {

    public IngVehiculoDAO(EntityManagerFactory emf) {
        this.emf = emf;
    }

    public IngVehiculoDAO() {
    }
    
    
        private EntityManagerFactory emf = Persistence.createEntityManagerFactory("my_persistence_unit");

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(IngVehiculo ingVehiculo) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(ingVehiculo);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findIngVehiculo(ingVehiculo.getPatente()) != null) {
                throw new PreexistingEntityException("IngVehiculo " + ingVehiculo + " already exists.", ex);                 
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(IngVehiculo ingVehiculo) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            ingVehiculo = em.merge(ingVehiculo);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = ingVehiculo.getPatente();
                if (findIngVehiculo(id) == null) {
                    throw new NonexistentEntityException("The ingVehiculo with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            IngVehiculo ingVehiculo;
            try {
                ingVehiculo = em.getReference(IngVehiculo.class, id);
                ingVehiculo.getPatente();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The ingVehiculo with id " + id + " no longer exists.", enfe);
            }
            em.remove(ingVehiculo);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<IngVehiculo> findIngVehiculoEntities() {
        return findIngVehiculoEntities(true, -1, -1);
    }

    public List<IngVehiculo> findIngVehiculoEntities(int maxResults, int firstResult) {
        return findIngVehiculoEntities(false, maxResults, firstResult);
    }

    private List<IngVehiculo> findIngVehiculoEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(IngVehiculo.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public IngVehiculo findIngVehiculo(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(IngVehiculo.class, id);
        } finally {
            em.close();
        }
    }

    public int getIngVehiculoCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<IngVehiculo> rt = cq.from(IngVehiculo.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
