/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.model.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author rlabarca
 */
@Entity
@Table(name = "ingvehiculos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "IngVehiculo.findAll", query = "SELECT i FROM IngVehiculo i"),
    @NamedQuery(name = "IngVehiculo.findByPatente", query = "SELECT i FROM IngVehiculo i WHERE i.patente = :patente"),
    @NamedQuery(name = "IngVehiculo.findByMarca", query = "SELECT i FROM IngVehiculo i WHERE i.marca = :marca"),
    @NamedQuery(name = "IngVehiculo.findByModelo", query = "SELECT i FROM IngVehiculo i WHERE i.modelo = :modelo"),
    @NamedQuery(name = "IngVehiculo.findByKms", query = "SELECT i FROM IngVehiculo i WHERE i.kms = :kms"),
    @NamedQuery(name = "IngVehiculo.findByAnho", query = "SELECT i FROM IngVehiculo i WHERE i.anho = :anho"),
    @NamedQuery(name = "IngVehiculo.findByRutDuenho", query = "SELECT i FROM IngVehiculo i WHERE i.rutDuenho = :rutDuenho")})
public class IngVehiculo implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "patente")
    private String patente;
    @Size(max = 25)
    @Column(name = "marca")
    private String marca;
    @Size(max = 25)
    @Column(name = "modelo")
    private String modelo;
    @Column(name = "kms")
    private Integer kms;
    @Column(name = "anho")
    private Integer anho;
    @Size(max = 10)
    @Column(name = "rut_duenho")
    private String rutDuenho;

    public IngVehiculo() {
    }

    public IngVehiculo(String patente) {
        this.patente = patente;
    }

    public String getPatente() {
        return patente;
    }

    public void setPatente(String patente) {
        this.patente = patente;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public Integer getKms() {
        return kms;
    }

    public void setKms(Integer kms) {
        this.kms = kms;
    }

    public Integer getAnho() {
        return anho;
    }

    public void setAnho(Integer anho) {
        this.anho = anho;
    }

    public String getRutDuenho() {
        return rutDuenho;
    }

    public void setRutDuenho(String rutDuenho) {
        this.rutDuenho = rutDuenho;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (patente != null ? patente.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof IngVehiculo)) {
            return false;
        }
        IngVehiculo other = (IngVehiculo) object;
        if ((this.patente == null && other.patente != null) || (this.patente != null && !this.patente.equals(other.patente))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "root.model.entities.IngVehiculo[ patente=" + patente + " ]";
    }
    
}
